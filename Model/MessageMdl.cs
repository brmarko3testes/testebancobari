﻿using System;
using System.IO;
using System.Text;

namespace MsgRedirectWorm.Model
{
	public class MessageMdl
	{
		public MessageMdl() { }

		public MessageMdl(MicroserviceMdl source, string message)
		{
			Source = source;
			Message = message;
			TimeStamp = DateTime.Now;
		}

		public MicroserviceMdl Source { get;  set; }
		public string Message { get; set; }
		public DateTime TimeStamp { get; set; }

		public override string ToString()
		{
			var result = new StringBuilder();

			result.AppendLine($" - Mensagem recebida de");
			result.AppendLine($" -- [Nome: {Source.Name}]");
			result.AppendLine($" -- [ID: {Source.Id}]");
			result.AppendLine($" -- [Enviado às {TimeStamp.ToString("F")}]");
			result.AppendLine($" - MSG: [{Message}]");

			return result.ToString();
		}
	}
}
