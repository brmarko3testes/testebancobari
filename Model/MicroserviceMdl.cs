﻿using System;

namespace MsgRedirectWorm.Model
{
	public class MicroserviceMdl
	{
		public MicroserviceMdl() =>
			Id = Guid.NewGuid();

		public Guid Id { get; set; }
		public string Name { get; set; }
	}
}
