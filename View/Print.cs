﻿using System;
using System.Text.RegularExpressions;

namespace MsgRedirectWorm.View
{
	// Adaptei um "front" que criei pra usar na faculdade.
	public static class Print
	{
		public static string CommonRegexLettersOnly = @"[a-zA-Z]";
		public static string CommonRegexNumbesOnly = @"\d+";

		public static ConsoleColor LineDefaultColor = ConsoleColor.Black;
		public static ConsoleColor AskColor = ConsoleColor.DarkGreen;
		public static ConsoleColor ErrorColor = ConsoleColor.DarkRed;

		public static void Line(string msg = null, ConsoleColor color = ConsoleColor.Black)
		{
			Console.BackgroundColor = color != LineDefaultColor ? color : LineDefaultColor;

			Console.WriteLine(msg);

			Console.BackgroundColor = LineDefaultColor;
		}

		public static bool TryAskString(string msg, out string result, string regexValidation = null)
		{
			result = null;
			Line(msg, AskColor);
			Console.Write(" >");
			var answer = Console.ReadLine();
			if (!string.IsNullOrWhiteSpace(regexValidation) && Regex.IsMatch(answer, regexValidation))
			{
				result = answer;
				return true;
			}
			return false;
		}

		public static string RequiredAskString(string question, string validationMsg = null, string regexValidation = null)
		{
			if (string.IsNullOrEmpty(regexValidation) && string.IsNullOrWhiteSpace(validationMsg))
			{
				Error(new Exception("É obrigatório preencher o validationMsg caso exista uma regexValidation."));
				return null;
			}

			var answer = string.Empty;
			do
			{
				if (!TryAskString(question, out answer, regexValidation))
					Error(new Exception("Não entendi sua resposta e preciso entendê-la: " + validationMsg));
				else
					return answer;

			} while (true);
		}

		public static bool YesNoQuestion(string msg)
		{
			Line(msg + " (s/n)", AskColor);
			var result = Console.ReadKey().KeyChar.ToString().ToLower() == "s";
			Line();
			return result;
		}

		// Se o sistema fosse um pouquinho maior, teria usado ValidationResult ao invés de usar Exceptions.
		public static void Error(Exception e)
		{
			Line("/!\\ Erro encontrado: " + e.Message, ErrorColor);
			Line();
		}
	}
}
