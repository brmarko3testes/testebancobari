﻿using MsgRedirectWorm.Core;
using MsgRedirectWorm.Model;
using MsgRedirectWorm.View;
using System;
using System.Threading;

namespace MsgRedirectWorm
{
	public class Program
	{
		private static MicroserviceMdl This = new MicroserviceMdl();

		private static void Main(string[] args)
		{
			Print.Line(" -- Setup --", ConsoleColor.DarkCyan);
			Print.Line();
			Print.Line("Oi, meu Id é: " + This.Id.ToString());
			Print.Line("Mas ainda não sei quem sou");
			Print.Line();
			Print.Line("(O nome do bot também é o nome da fila que ele cuidará)");
			This.Name = Print.RequiredAskString("Digite um nome pra esse bot", "Somente letras aqui!", Print.CommonRegexLettersOnly);

			var target = string.Empty;
			if (Print.YesNoQuestion("Agora, devo enviar uma mensagem a cada 5 segundos?"))
			{
				Print.Line("Okay, enviarei \"Hello world\" a cada 5 segundos, mas pra quem?");
				target = Print.RequiredAskString("Digite o nome do alvo", "Somente letras aqui!", Print.CommonRegexLettersOnly);
			} else
				Print.Line("Nesse caso, fico só escutando minha fila aqui...");

			Print.Line();
			Print.Line("Coloquei output na tela pra tudo, sendo assim, limitei pra");
			Print.Line("ENVIAR uma mensagem por segundo e");
			Print.Line("RECEBER um a cada 5 segundos (conforme a documentação)");
			Print.Line("Os valores de tempo podem ser alterados no método StartLoop()");
			Print.Line();
			Print.YesNoQuestion("Posso iniciar o main loop?");

			StartLoop(target);
		}

		private static void StartLoop(string msgsTarget)
		{
			Print.Line(This.Name + ": Iniciando loop principal:", ConsoleColor.DarkCyan);

			var checkNewMessagesFreq = 1;   // Verifica por novas mensagens a cada 1 segundo;
			var sendMessageFreq = 5;    // Envia uma nova mensagem a cada 5 segundos;

			// Timer pra verificar por novas mensagens recebidas:
			var msgLastCheckTime = DateTime.Now.AddYears(-10);

			// Timer pra enviar mensagens:
			var msgLastSendTime = DateTime.Now.AddYears(-10);

			while (true)
			{
				// Checks for arriving messages:
				if (msgLastCheckTime <= DateTime.Now.AddSeconds(0 - checkNewMessagesFreq))
				{
					new Thread(delegate ()
					{
						try
						{
							if (new Messager().TryCheckIncoming(This.Name, out var incomingMsg))
							{
								Print.Line(" - Mensagem recebida:", ConsoleColor.DarkMagenta);
								Print.Line(incomingMsg.ToString());
							} else
							{
								Print.Line(" - Nada pra mim na fila...", ConsoleColor.DarkMagenta);
							}
						} catch (Exception e)
						{
							Print.Error(e);
						}
					}).Start();
					msgLastCheckTime = DateTime.Now;
				}

				// Send a message:
				if (!string.IsNullOrWhiteSpace(msgsTarget) && msgLastSendTime <= DateTime.Now.AddSeconds(0 - sendMessageFreq))
				{
					new Thread(delegate ()
					{
						try
						{
							Print.Line(new Messager().Send(msgsTarget, new MessageMdl(This, "Hello World")), ConsoleColor.DarkBlue);
						} catch (Exception e)
						{
							Print.Error(e);
						}
					}).Start();
					msgLastSendTime = DateTime.Now;
				}
			}
		}
	}
}
