﻿using RabbitMQ.Client;

namespace MsgRedirectWorm.Core
{
	public static class RabbitSingleton
	{
		private static string Coelho = "localhost";
		private static ConnectionFactory _Connection { get; set; }

		public static ConnectionFactory Connection()
		{
			_Connection = _Connection ?? new ConnectionFactory() { HostName = Coelho };

			return _Connection;
		}
	}
}
