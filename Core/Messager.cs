﻿using MsgRedirectWorm.Model;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Exceptions;
using System;

namespace MsgRedirectWorm.Core
{
	public class Messager
	{
		public bool TryCheckIncoming(string queueName, out MessageMdl result)
		{
			var returning = new MessageMdl();
			var isValid = false;

			using (var channel = RabbitSingleton.Connection().CreateConnection().CreateModel())
			{
				QueueDeclare(channel, queueName);

				var consumer = new EventingBasicConsumer(channel);
				consumer.Received += (model, ea) =>
				{
					if (MessageConverter.TryParseMessageMdl(ea.Body.ToArray(), out var _parsed))
					{
						returning = _parsed;
						channel.BasicAck(ea.DeliveryTag, false);
						isValid = true;
					} else
					{
						returning = null;
						isValid = false;
					}
				};
				var c = channel.BasicConsume(queueName, true, consumer);
			}

			result = isValid ?
				returning :
				null;

			return isValid;
		}

		public string Send(string target, MessageMdl metaData)
		{
			var result = string.Empty;
			var msgNotSent = "Mensagem não enviada; ";
			// Como estou acostumado a fazer projetos pra fora, usualmente, esse tipo de var (msgNotsent),
			// coloco num arquivo externo e importo com ConfigurationManager.

			try
			{
				using (var channel = RabbitSingleton.Connection().CreateConnection().CreateModel())
				{
					QueueDeclare(channel, target);
					channel.BasicPublish("default", "", null, MessageConverter.ToByteArray(metaData));
					result = $"Enviei: \"{metaData.Message}\" para \"{target}\"";
				}
			} catch (ArgumentNullException)
			{
				throw new Exception(msgNotSent + "A mensagem estava vazia.");
			} catch (BrokerUnreachableException)
			{
				throw new Exception(msgNotSent + "Não pude encontrar o broker.");
			} catch (Exception)
			{
				throw new Exception(msgNotSent + "[" + metaData.TimeStamp + "]: " + metaData.Message);
			}

			return result;
		}

		private void QueueDeclare(IModel channel, string queueName) => 
			channel.QueueDeclare(queueName, false, false, false, null);
	}
}
