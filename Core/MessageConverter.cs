﻿using MsgRedirectWorm.Model;
using System;
using System.IO;

namespace MsgRedirectWorm.Core
{
	public static class MessageConverter
	{
		public static byte[] ToByteArray(MessageMdl msg)
		{
			using (var m = new MemoryStream())
			{
				using (var writer = new BinaryWriter(m))
				{
					writer.Write(msg.Source.Id.ToString());
					writer.Write(msg.Source.Name);
					writer.Write(msg.Message);
					writer.Write(msg.TimeStamp.ToFileTimeUtc());
				}
				return m.ToArray();
			}
		}

		public static bool TryParseMessageMdl(byte[] data, out MessageMdl result)
		{
			result = new MessageMdl();
			try
			{
				using (var m = new MemoryStream(data))
				{
					using (var reader = new BinaryReader(m))
					{
						var _idParsed = Guid.TryParse(reader.ReadString(), out var id);
						result.Source = new MicroserviceMdl()
						{
							Name = reader.ReadString(),
						};
						result.Source.Id = _idParsed ? id : Guid.Empty;
						result.Message = reader.ReadString();
						result.TimeStamp = new DateTime(reader.ReadInt32());
						return true;
					}
				}
			} catch (Exception)
			{
				return false;
			}
		}
	}
}
