﻿Versões dos Softwares utilizados no desenvolvimento:

Visual Studio:
- Visual Studio 2017;
- .NET Core 2.1;
- Bibliotecas:
-- RabbitMQ C# Client 6.2.1;

RabbitMQ:
- RabbitMQ-server 3.6.6;
- Erlang8.1;